# Hooks

## ngOnChanges

    Propietat amb tipo primitiu canvia

## ngDoCheck

    Canvis en propietats internes o coleccions

## afterViewInit

    Després de inicialitzar la vista

## ngOnDestroy

    Al sortir d'un component

    