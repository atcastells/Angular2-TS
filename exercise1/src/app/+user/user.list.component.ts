import { UserService } from '../api/services';
import { UserModel } from '../api/models';
import { Subscription } from 'rxjs/Rx';

import { Input, OnDestroy, OnInit } from '@angular/core/core';
import { Component } from '@angular/core';

@Component({
  selector: 'app-user-list',
  templateUrl: './user.list.component.html',
  styleUrls: ['./user.list.component.css']
})
export class UserListComponent implements OnInit, OnDestroy {
  private _subscription: Subscription
  title: String = 'Listado de usuarios';
  users: UserModel[];

  constructor(private _userService: UserService) {}

  ngOnInit() {
    this._subscription = this._userService.getUsers().subscribe((response) => {
      this.users = response.RestResponse.result;
    },
    (error) => {
      console.error(error)
    });
  }
  
  ngOnDestroy() {
    if (this._subscription) {
      this._subscription.unsubscribe()
    }
  }

}
