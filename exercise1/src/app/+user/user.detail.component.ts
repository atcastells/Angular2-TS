import { UserModel } from '../api/models';
import { UserService } from '../api/services';
import { Observable, Subscription } from 'rxjs/Rx';
import { ActivatedRoute, Router } from '@angular/router';
import { identifierModuleUrl } from '@angular/compiler';
import { Component } from '@angular/core';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user.detail.component.html',
  styleUrls: ['./user.detail.component.css'],
})
export class UserDetailComponent  {
  private _subscription: Subscription;
  title = 'user detail';
  userDetails: UserModel;
  id;

  constructor(private router: ActivatedRoute, private _userService: UserService) {
     router.params.subscribe((params) => {
       this.id = params.id
       this._subscription = this._userService.getUserById(this.id).subscribe((res) => {
          this.userDetails = res.RestResponse.result;
       })
     })
  }
}
