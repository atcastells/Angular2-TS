import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';


import { MaterialModule } from '@angular/material';
import { UserRoutingModule } from './user.routing.module';

import {UserDetailComponent} from './user.detail.component'
import {UserListComponent} from './user.list.component'

@NgModule({
    declarations: [
        UserDetailComponent,
        UserListComponent
    ],
    imports: [
        UserRoutingModule, 
        MaterialModule,
        CommonModule
    ],
})
export class UserModule { }