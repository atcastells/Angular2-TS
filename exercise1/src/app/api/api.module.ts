import { NgModule } from '@angular/core';
import { UserService } from  './services';

@NgModule({
    imports: [],
    providers: [UserService]
})
export class ApiModule {}