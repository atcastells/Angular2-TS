import { Observable } from 'rxjs/Rx';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';


@Injectable() 
export class UserService {
    constructor(private _http: Http) {}

    getUsers(): Observable<any> {
        return this._http.get('http://services.groupkt.com/country/get/all')
            .map(res => res.json() as any)
            .catch(error => Observable.throw(error));
    }

    getUserById(code: string): Observable<any> {
        return this._http.get(`http://services.groupkt.com/country/get/iso2code/${code}`)
        .map(res => res.json() as any)
        .catch(error => Observable.throw(error));
    }
}